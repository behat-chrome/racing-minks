# Racing Minks 🚗🚗🏁

This claim sits in the README of dmore/chrome-mink-driver:

> It communicates directly with chrome over HTTP and WebSockets, which allows it to work at least twice as fast as chrome with selenium.
For chrome 59+ it supports headless mode, eliminating the need to install a display server, and the overhead that comes with it.
This driver is tested and benchmarked against a behat suite of 1800 scenarios and 19000 steps. It can successfully run it in less than 18 minutes with chrome 60 headless.
The same suite running against chrome 58 with xvfb and selenium takes ~60 minutes. ([dmore/chrome-mink-driver README, April 2017](https://gitlab.com/behat-chrome/behat-chrome-extension/))

I thought we'd better see if that applies today. We can do this in Gitlab CI by running the same test suite against multiple drivers and compare run times.

